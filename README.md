# nodecg-weaver

*nodecg-weaver* is a bundle for nodecg providing functions to quickly and easily
generate a web API.

This project contains the bundle and an example to show how to use it.

## Features

- A simple interface to build web API endpoints built on top of expressjs (that
  nodecg exposes).
- A quick and dirty documentation generation for each endpoints to display the
  API reference to help the end user build its scripts.

## Use case

The idea behind the bundle comes from me seeing the production team on an
event with tools such as elgato touchpad or equivalent things. Most teams use
buttons for automation and simplification of several tasks to make the work
easier.

I've learned you can bind script to keys for most of those kind of tools, and I
believe this can be useful to perform nodecg manipulation, and one way to do it
would be to make calls to a web REST API; especially if the nodecg instance is
remote.

## Dependencies

Since nodecg-weaver is a bundle of nodecg, please install nodecg first.

You can find more information on this link: [nodecg](https://www.nodecg.dev/).

There are no other dependencies.

## Installation

Simply copy the directory `source/bundles/nodecg-weaver` into your own nodecg
`/bundles` directory and then run the server.

## Usage

### Hello world example:

In your bundle, in `/extension/index.js`:

```js
module.exports = nodecg =>
{
    /* Grab the bundle "library". */
    const Weaver = nodecg.extensions["nodecg-weaver"];

    /* First, declare a gateway.
     * This sets up a base route for all your endpoints inside the gateway, in
     * case you wish to expose several different APIs. */
    const gateway = new Weaver.Gateway("/api/v1");

    /* Add an endpoint to the gateway. */
    gateway.addEndpoint(new Weaver.Endpoint("GET",
        "/hello-world",
        (request, response) =>
        {
            response.send({message: "Hello world!"});
        });
};
```

Run nodecg.

In a terminal, afterward (assuming you are running nodecg on `localhost:9090`):

```bash
curl -X GET 'http://localhost:9090/api/v1/hello-world' -H 'Accept: application/json'
```

The result should be `{"message":"Hello world!"}`.

### More

The project includes a slightly more realistic example in the directory
`/example/bundles/weaver-example` alondside the file
`example/browser/index.html`.

Simply:
- Copy the directory `/example/bundles/weaver-example` into you own nodecg
  `/bundles` directory
- Run nodecg (or restart it)
- Open the file `example/browser/index.html` in a navigator
  and play with it. This file "simulates" a touchpad, to give you a more clear
  idea.

**Note**: This example assumes your instance of nodecg runs on `localhost:9090`.

## Potential improvements

- Automatic validation of the payload object for endpoints requiring it;
- Better documentation generation;
- Adding the possibility of installing via nodecg-cli;
- Tests?

## License

The project is under [zlib license](https://en.wikipedia.org/wiki/Zlib_License).

## Project status

There are no plan to develop the improvements anytime soon. This is a very
small proof of concept of a very niche need in a very niche domain after all.
