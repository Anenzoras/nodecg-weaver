/*
 *  Date:   2022-08-14
 *
 *  Brief:  Rough type validator asserter.
 *
 *  License: zlib
 */

"use strict";

(function ()
{
    /**
     *  @brief
     *      Used as a namespace.
     *
     *  @example
     *      ```
     *          Validator.prepare("aFunction()")
     *              .assert(parameter1, "string", "parameter1", "string")
     *              .assert(parameter2, AType, "parameter2", "AType");
     *      ```
     *      An assert fails if its first parameter is either `undefined`,
     *      `null` or not of the given type in the second parameter. The other
     *      parameters are for when an assert fails; it throws a TypeError with
     *      a nice message saying the fail happened regarding the specific
     *      parameter, expecting the specific type into "aFunction()" (in this
     *      example).
     */
    let Validator = class {};

    /**
     *  @brief
     *      This class exposes an `assert()` function, which is used for the
     *      checking.
     */
    let _Validator = class
    {
        constructor ()
        {
            this.location = "";
        }

        /**
         *  @brief
         *      Assert that the given variable is of given type, and throws an
         *      exception if not.
         *
         *  @param {Any} variable
         *      The variable to check for.
         *  @param {String || Function} type
         *      The type to check against. Might be a native type, a class or a
         *      constructor function.
         *  @param {String} variableName
         *      The name of the variable checked, for a pretty exception
         *      message.
         *  @param {String} typeName
         *      The name of the type checked against, for a pretty exception
         *      message.
         *
         *  @throw {TypeError}
         *      If `variable` and `type` don't match, the function throws a
         *      `TypeError` with a message using `this.location`,
         *      `variableName` and `typeName`.
         *
         *      On the case any of the given parameter is invalid in itself for
         *      the assert function (e.g. `variableName` is not a string) it
         *      also returns a type error, but the exception message reflects
         *      that.
         *
         *  @return {_Validator}
         *      If the function does not fail, it returns its own object, to
         *      chain the calls.
         */
        assert(variable, type, variableName, typeName)
        {
            /*
             * Function self sanity check
             */
            if (type === undefined
                || type === null
                || !(typeof type === "string"
                    || typeof type === "function"))
            {
                throw new TypeError("[_Validator.assert()]"
                    + " parameter `type` must be a valid type indicator"
                    + " (string or function).");
            }

            if (variableName === undefined
                || variableName === null
                || typeof variableName !== "string")
            {
                throw new TypeError("[_Validator.assert()]"
                    + " parameter `variableName` must be a valid string.");
            }

            if (typeName === undefined
                || typeName === null
                || typeof typeName !== "string")
            {
                throw new TypeError("[_Validator.assert()]"
                    + " parameter `typeName` must be a valid string.");
            }

            /*
             * Actual function execution
             */
            if (this.location === undefined || this.location === null)
            {
                this.location = "";
            }

            const errorString = "[" + this.location + "]"
                + " `" + variableName + "` must not be null nor undefined and"
                + " must be a valid `" + typeName + "`.";

            if (variable === undefined || variable === null)
            {
                throw new TypeError(errorString);
            }

            if (typeof type === "string")
            {
                /* Native type */
                if (typeof variable !== type)
                {
                    throw new TypeError(errorString);
                }
            }
            else if (typeof type === "function")
            {
                /* Classes and stuff. */

                if (type === Array)
                {
                    /* Check for array first, since its stupid. */
                    if (!Array.isArray(variable))
                    {
                        throw new TypeError(errorString);
                    }
                }
                else
                {
                    if (!(variable instanceof type))
                    {
                        throw new TypeError(errorString);
                    }
                }
            }

            return this;
        }
    };

    /**
     *  @property {_Validator} instance
     *      There is only a need for one instance of this object.
     */
    _Validator.instance = new _Validator();

    /**
     *  @brief
     *      Prepares the underlying validator object.
     *
     *  @param {String} location
     *      The location of the call to this function.
     *
     *  @return {_Validator}
     *      A type validator object, to call `assert()` with.
     */
    Validator.prepare = function prepare(location)
    {
        _Validator.instance.location = "Validator.prepare()";
        _Validator.instance.assert(location, "string", "location", "string");

        _Validator.instance.location = location;

        return _Validator.instance;
    };

    module.exports = Validator;
})();
