"use strict";

const Validator = require("./mlsl/validator.js");

module.exports = nodecg =>
{
    const Weaver = {};

    /**
     *  @brief
     *      Small class to keep all info of an endpoint in a single place.
     */
    Weaver.Endpoint = class
    {
        /**
         *  @brief
         *      Builds an enpoint object.
         *
         *  @param {string} [in] method
         *      The http method of the endpoint. Is expected to be a method
         *      accepted by `express`, which also includes "all".
         *  @param {string} [in] route
         *      The route of the endpoint.
         *  @param {function} [in] callback
         *      The callback function when the endpoint is reached.
         *  @param {string} [in] <optional> name
         *      The name of the endpoint, to generate a documentation.
         *  @param {string} [in] <optional> description
         *      The description of the endpoint, to generate a documentation.
         *  @param {object} [in] <optional> payloadDescription
         *      An object description of the payload given to the endpoint, to
         *      generate a documentation.
         *  @param {object} [in] <optional> responseDescription
         *      An object description of the response of the endpoint, to
         *      generate a documentation.
         *
         *  @throw {TypeError}
         *      If:
         *      - `route` was not given or not a string;
         *      - `method` was not given or not a string;
         *      - `callback` was not given or not a function;
         *      - `name` was given but not a string;
         *      - `description` was given but not a string;
         *      - `payloadDescription` was given but not an object;
         *      - `responseDescription` was given but not an object;
         */
        constructor (method,
            route,
            callback,
            name,
            description,
            payloadDescription,
            responseDescription)
        {
            const _name = name || "";
            const _description = description || "";
            const _payloadDescription = payloadDescription || {};
            const _responseDescription = responseDescription || {};

            Validator.prepare("Weaver.Endpoint()")
                .assert(route, "string", "route", "string")
                .assert(method, "string", "method", "string")
                .assert(callback, "function", "callback", "function")
                .assert(_name, "string", "name", "string")
                .assert(_description, "string", "description", "string")
                .assert(_payloadDescription, "object", "description", "object")
                .assert(_responseDescription, "object", "description", "object");

            /**
             *  @property {string} method
             *      The HTTP method of the endpoint.
             */
            this.method = method.toLowerCase();
            /**
             *  @property {string} route
             *      The end route of the endpoint.
             */
            this.route = route;
            /**
             *  @property {function} callback
             *      The callback that'll process requests.
             */
            this.callback = callback;
            /**
             *  @property {string} name
             *      The name of the endpoint. For documentation purposes.
             */
            this.name = _name;
            /**
             *  @property {string} description
             *      The description of the endpoint. For documentation purposes.
             */
            this.description = _description;
            /**
             *  @property {object} payloadDescription
             *      An object description of an example payload of the
             *      endpoint. For documentation purposes.
             */
            /* Yes, if not given, do not use the intermediary variable, make it
             * null. It is easier to manage this way. */
            this.payloadDescription = payloadDescription || null;
            /**
             *  @property {string} responseDescription
             *      An object description of an example response from the
             *      endpoint. For documentation purposes
             */
            /* Yes, if not given, do not use the intermediary variable, make it
             * null. It is easier to manage this way. */
            this.responseDescription = responseDescription || null;
        }
    }

    /**
     *  @brief
     *      Gateway is the base holder for the API. This allow for several API
     *      to coexist with different base path, which is quite practical if two
     *      bundles uses different paths for their request, or if there are
     *      several version to an API.
     *
     *      (El classicò "/api/v1" and "api/v2" stuff).
     */
    Weaver.Gateway = class
    {
        /**
         *  @brief
         *      Builds a gateway.
         *
         *  @param {string} [in] gatewayPath
         *      The base path for the api. For instance "/api/v1".
         *      Must be a valid path for `express`.
         *
         *  @throw {TypeError}
         *      If `gatewayPath` wasn't provided or is not a string.
         */
        constructor (gatewayPath)
        {
            Validator.prepare("Weaver.Gateway()")
                .assert(gatewayPath, "string", "gatewayPath", "string");

            /**
             *  @property {string} path
             *      Base path of all the child endpoints.
             */
            this.path = gatewayPath;
            /**
             *  @property {express.Router} router
             *      The router associated with the gateway.
             *      Can eventually be used to add middleware processing to the
             *      requests.
             */
            this.router = nodecg.Router();
            /**
             *  @property {Weaver.Endpoint[][]} endpointBag
             *      Associative array of associative array of endpoints, for
             *      storage.
             *
             *      The first key is the method; the second one is the route.
             */
            this.endpointBag = [];

            nodecg.mount(this.path, this.router);
        }

        /**
         *  @brief
         *      Adds an endpoint to the gateway.
         *
         *      If the route and the method already exist, nothing happens
         *      (`express` does not override nor remove routes).
         *
         *  @param {Weaver.Endpoint} endpoint
         *      The endpoint to add to the gateway.
         *
         *  @throw {TypeError}
         *      If `endpoint` is `null`, `undefined` or not an instance of
         *      `Weaver.Endpoint`.
         *  @throw {Error}
         *      If `method` is not a valid express (http) method.
         */
        addEndpoint(endpoint)
        {
            Validator.prepare("Weaver.Gateway.addEndpoint()")
                .assert(endpoint, Weaver.Endpoint, "endpoint", "Weaver.Endpoint");

            /* Check if the method is valid for express. */
            {
                let _method = endpoint.method.toLowerCase();

                if (typeof this.router[_method] !== "function")
                {
                    throw new Error("[Weaver.Gateway.addEndpoint()]"
                        + " `method` is not a valid express (http) method.");
                }

                /* Also check the route doesn't already exists */

                if (this.endpointBag[_method] && this.endpointBag[_method][endpoint.route])
                {
                    /* Endpoint already exists, nothing happen */
                    console.info("[Weaver.Gateway.addEndpoint()]"
                        + " route `" + route + "` with method `" + method
                        + "` already exists.");

                    return;
                }
            }

            /* Everything is valid here, let's go. */

            this.endpointBag[endpoint.method] = this.endpointBag[endpoint.method] || [];
            this.endpointBag[endpoint.method][endpoint.route] = endpoint;

            this.router[endpoint.method](endpoint.route, endpoint.callback);
        }

        /**
         *  @brief
         *      Find an enpoint from the given method and route and
         *      returns a string that can be used to generate DOM content in the
         *      front end for documentation purposes.
         *
         *      If no endpoint was found, returns an empty string.
         *
         *  @param {string} [in] method
         *      The HTTP method of the endpoint.
         *  @param {string} [in] route
         *      THe HTTP route of the endpoint.
         *
         *  @return {string}
         *      A generated string for the documentation.
         *
         *  @throw {TypeError}
         *      If either:
         *      - `method` is `null`, `undefined` or not a string;
         *      - `route` is `null`, `undefined` or not a string;
         */
        generateHTMLReferenceOf(method, route)
        {
            Validator.prepare("Weaver.Endpoint.generateHTMLDocumentationOf()")
                .assert(method, "string", "method", "string")
                .assert(route, "string", "route", "string");

            const _method = method.toLowerCase();

            if (this.endpointBag[_method] === undefined
                || this.endpointBag[_method] === null
                || this.endpointBag[_method][route] === undefined
                || this.endpointBag[_method][route] === null)
            {
                return "";
            }

            const endpoint = this.endpointBag[_method][route];

            let documentationHTML = "";
            let nameString = "";
            let descriptionString = "";
            let curlExampleString = "";
            let javascriptExampleString = "";
            let payloadString = "";
            let responseString = "";

            nameString = endpoint.name === ""
                ? "<em>Anonymous endpoint</em>"
                : endpoint.name;

            descriptionString = endpoint.description === ""
                ? ""
                : "<p>" + endpoint.description + "</p>";

            /* Example calls. */
            {
                /* @Incomplete: find a nice way to print the actual base URL. */
                const FULL_URL = "_NODECG_BASE_URL_"
                    + this.path
                    + endpoint.route;

                /*
                 * Curl
                 */

                curlExampleString = "curl -X "
                    + endpoint.method.toUpperCase()
                    + " '"
                    + FULL_URL
                    + "'\n";

                if (endpoint.responseDescription !== null)
                {
                    curlExampleString += "    -H 'Accept: application/json'\n";
                }

                if (endpoint.payloadDescription !== null)
                {
                    curlExampleString += "    -H 'Content-Type: application/json'\n";
                    curlExampleString += "    -d '";
                    curlExampleString += JSON.stringify(endpoint.payloadDescription);
                    curlExampleString += "'\n";
                }

                /*
                 * Javascript (fetch)
                 */
                let hasHeader = false;
                let hasBody = false;
                let headerString = "new Headers([";
                let bodyString = "null";
                let callbackString = "";

                if (endpoint.payloadDescription !== null)
                {
                    headerString += "['Content-Type', 'application/json'],";
                    bodyString = "'" + JSON.stringify(endpoint.payloadDescription) + "'";

                    hasHeader = true;
                    hasBody = true;
                }

                if (endpoint.responseDescription !== null)
                {
                    headerString += "['Accept', 'application/json'],";
                    callbackString = "\n"
                        + "    .then((response) =>\n"
                        + "        {\n"
                        + "            return response.json();\n"
                        + "        })\n"
                        + "    .then((value) =>\n"
                        + "        {\n"
                        + "            console.log(value);\n"
                        + "        })";

                    hasHeader = true;
                }

                headerString += "])";

                javascriptExampleString = ""
                    + `fetch("${FULL_URL}",\n`
                    + `    {\n`
                    + `        method: "${endpoint.method.toUpperCase()}",\n`
                    + (hasHeader ? `        headers: ${headerString},\n`: "")
                    + (hasBody ? `        body: ${bodyString},\n` : "")
                    + `    })${callbackString};\n`;
            }

            /* Payload */
            if (endpoint.payloadDescription === null)
            {
                payloadString = "None expected";
            }
            else
            {
                payloadString = JSON.stringify(endpoint.payloadDescription, null, 2);
            }

            /* Response */
            if (endpoint.responseDescription === null)
            {
                responseString = "None returned";
            }
            else
            {
                responseString = JSON.stringify(endpoint.responseDescription, null, 2);
            }

            documentationHTML = ""
                + `<h2>${nameString}</h2>\n`
                + `${descriptionString}\n`
                + `<p>Example using curl:</p>\n`
                + `<pre>${curlExampleString}</pre>\n`
                + `<p>Example using javascript:</p>\n`
                + `<pre>${javascriptExampleString}</pre>\n`
                + `<p>Payload:</p>\n`
                + `<pre>${payloadString}</pre>\n`
                + `<p>Response:</p>\n`
                + `<pre>${responseString}</pre>\n`;

            return documentationHTML;
        }
    };

    return Weaver;
};
