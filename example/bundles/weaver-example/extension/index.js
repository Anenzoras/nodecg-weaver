"use strict";

module.exports = nodecg =>
{
    const Weaver = nodecg.extensions["nodecg-weaver"];

    /* For testing purpose. The message is displayed in the graphics. */
    const weMessageReplicant = nodecg.Replicant('weMessage',
        {defaultValue: "Weaver example message!"});

    /* First, declare a gateway. */
    const gateway = new Weaver.Gateway("/api/v1");

    /* For our specific example, we need to avoid CORS problems, thus we modify
     * the router in the gateway.
     * This also shows you can add stuff to the router itself if need be (like
     * http authentication for request or something). */
    gateway.router.use((request, response, next) =>
    {
        response.append('Access-Control-Allow-Origin', ['*']);
        response.append('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE');
        response.append('Access-Control-Allow-Headers', 'Content-Type');

        next();
    });

    /* Our series of endpoints.
     *
     *  This example provides 5 endpoints to display the possibilities of the
     *  library.
     *
     *  In the dashboard, here's the possible example actions:
     *  - modify a message in the graphics;
     *  - display or hide the "omnibar";
     *  - briefly light up a small signal in the dashboard (blip a diode);
     *  - briefly light up a small signal in the graphics (blip another diode).
     *  For each actions, an endpoint has been created to do the same.
     *  Additionnaly, there is an endpoint to retrieve the number of endpoint,
     *  just for the heck of it.
     */
    const endpoints =
    [
        new Weaver.Endpoint("GET",
            "/count",
            (request, response) =>
            {
                response.send({count: endpoints.length});
            },
            "Count",
            "Returns the number of endpoints in this example bundle",
            null,
            {
                "count": 0,
            }),
        new Weaver.Endpoint("POST",
            "/dashboard-blip",
            (request, response) =>
            {
                nodecg.sendMessage("weaver-example-dashboard-blip");

                response.send();
            },
            "Dashboard Blip", // listen to animation end
            "This endpoint triggers a little blip signal on the dashboard.",
            null,
            null),
        new Weaver.Endpoint("POST",
            "/graphics-blip",
            (request, response) =>
            {
                nodecg.sendMessage("weaver-example-graphics-blip");

                response.send();
            },
            "Graphics Blip",
            "This endpoint triggers a little blip signal on the graphics.",
            null,
            null),
        new Weaver.Endpoint("POST",
            "/toggle-omnibar",
            (request, response) =>
            {
                nodecg.sendMessage("weaver-example-toggle-omnibar");

                response.send();
            },
            "Toggle omnibar",
            "Toggles the \"omnibar\" on the graphics.",
            null,
            null),
        new Weaver.Endpoint("PUT",
            "/omnibar-message",
            (request, response) =>
            {
                // const payload = JSON.parse(request.body);
                const payload = request.body

                const oldMessage = weMessageReplicant.value;

                if (payload && payload.newMessage)
                {
                    weMessageReplicant.value = payload.newMessage;
                }

                response.send({previousMessage: oldMessage});
            },
            "Change omnibar message",
            "Changes the  \"omnibar\"'s message on the graphics and also returns the previous one.",
            {
                "newMessage": "message",
            },
            {
                "previousMessage": "message",
            }),
    ];

    for (let endpoint of endpoints)
    {
        gateway.addEndpoint(endpoint);
    }

    /* When the dashboard loads, it fires this event in order to retrieve the
     * documentation linked to each endpoints. The documentation is generated
     * by Weaver and is a string containing HTML notation. This can then be
     * added to the DOM in the dashboard, as done in this example to help the
     * end user in using the API.
     *
     * Note that the function is merely an helper, you could entirely design a
     * cleaner documentation by yourself if you feel the need, but it can help
     * for a quick and dirty set of endpoints. */
    nodecg.listenFor("weaver-example-loaded",
        (value, aknowledgement) =>
        {
            if (aknowledgement && !aknowledgement.handled)
            {
                let referenceItems = [];

                for (let endpoint of endpoints)
                {
                    let referenceItem = gateway.generateHTMLReferenceOf(
                        endpoint.method,
                        endpoint.route
                    );

                    referenceItems.push(referenceItem);
                }

                aknowledgement(null, referenceItems);
            }
        });
};
